---
layout: default
permalink: /debugging.html
title: "Debugging Information"
---

# Debugging Information

Sometimes there are behaviors or bugs that you can reliably reproduce but we cannot.  In these instances it is often helpful to have a full **debug.out** which contains all of the keystrokes you pressed, the accessibility events that were emitted, what was spoken and brailled as a result, and any errors that may have occurred.

## Obtaining a Debug Output File
There are different debugging levels and different options/strategies for obtaining a full **debug.out**.  We should outline them all here.  For now, however, here are two approaches:

**Option 1: Using Orca's Debug Command**

This is the simplest method that most users can follow for obtaining a full debug output.

* Press Alt+F2 to show the Run dialog
* Type `orca --debug` to start Orca in debug mode

The debug output file will be named in the format: `debug-YYYY-MM-DD-HH:MM:SS.out`. If you would like the file to have a name different from this format use the `--debug-file=name` command, for example: `orca --debug --debug-file=debug.txt`.

After you have captured the info you need be sure to restart Orca normally with: `orca` (or `orca -r`) so the file won't get too large.

**Option 2: Editing Your Orca Customizations**

In your "$HOME/.local/share/orca/orca-customizations.py" file (which may be empty), add the following lines:

```
import orca.debug
orca.debug.debugLevel = orca.debug.LEVEL_ALL

# If you do not specify a file, all output will go to
# 'stdout' where it can be redirected to a file.

import time
orca.debug.debugFile = open(time.strftime('debug-%Y-%m-%d-%H:%M:%S.out'), 'w')
```

To generate a debug output from this method:

1. Quit Orca.
2. Relaunch it and any relevant application(s).
3. Reproduce the problem in that same application(s)
4. Quit Orca again to produce a debug file.

This will make Orca produce a debugging output file in your home directory (or wherever you launched Orca from if you manually launch it). The file's name will begin with "debug" end with ".out" and have a time stamp in between.

To stop Orca from recording debugging information, edit that orca-customizations.py file once again and comment-out or remove those added lines. When you next relaunch Orca, things should be back to normal.

If you use Orca in order to perform the above edits to your orca-customizations.py the lines, you will have two debug.outs (the first from reproducing the problem; the second from recommenting the lines).  We of course need the first of the two. :-)

## Where To Send It

The resulting file will likely be quite large and thus rejected by the GNOME mailing list. You can instead send it directly to the developer who asked for it. Alternatively, you can attach it to a bug or make it available via a file-sharing service, pastebin, etc.