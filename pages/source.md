---
layout: default
permalink: /source.html
title: "Source Code"
---

# Source Code

Orca and its dependencies are already included in many Linux distributions. For more information and/or to get the code, please see the following:

 * [Orca GNOME GitLab page](https://gitlab.gnome.org/GNOME/orca/)

## Orca Dependencies

Source code for accessibility libraries used by Orca.
<ul class="tiles">
    <li>
        <a href="https://gitlab.gnome.org/GNOME/atk" title="Accessibility Toolkit project on the GNOME Gitlab">
        <h3>ATK</h3>
        <p>A library  that provides interface definitions for toolkits that wish to integrate with the GNOME accessibility infrastructure</p>
        </a>
    </li>
    <li>
        <a href="https://gitlab.gnome.org/GNOME/at-spi2-core/-/tree/main/" title="at-spi2-core project on the GNOME Gitlab">
        <h3>at-spi2-core</h3>
        <p>Base DBus XML interfaces for accessibility, the accessibility registry daemon, and atspi library.</p>
        </a>
    </li>
</ul>

Additional optional dependencies for Braille and speech output:

* [Speech Dispatcher](https://devel.freebsoft.org/speechd) - provides a device independent layer for access to speech synthesis.
* [BRLTTY](http://mielke.cc/brltty) - enables the use of a refreshable braille display while accessing a Linux/Unix console.
* [Liblouis](http://liblouis.io) - a translator, back-translator and formatter for a large number of languages and braille codes.
* [libspiel](https://project-spiel.org/) - a client library that offers an ergonomic speech interface through GObject Introspection.