---
layout: default
home: true
---

<br><br>
![Orca Logo - Orca whale with a white cane](assets/orca-256x256.png){: .center }

# Orca
{:.no-margin}

Orca is a free, open source, flexible, and extensible screen reader that provides access to the graphical desktop via user-customizable combinations of speech and/or braille.

Orca works with applications and toolkits that support the [Assistive Technology Service Provider Interface (AT-SPI)](https://www.freedesktop.org/wiki/Accessibility/AT-SPI2/), which is the primary assistive technology infrastructure for free and open desktops.


## Contributing

Orca is Free Software and is developed in the open, to learn how you can get involved visit the links below.

 * [Get Involved](./get-involved.html)
 * [Source Code](./source.html)

As a part of the GNOME Project, it can be useful to first look at the [GNOME Handbook](https://handbook.gnome.org/) and the documentation below first.

## Documentation
{: #documentation}

 * [Orca Users Guide](https://gnome.pages.gitlab.gnome.org/orca/help/)
 * [Using Orca's Preferences GUI](https://gnome.pages.gitlab.gnome.org/orca/help/preferences.html)
 * [Using Orca's Keyboard Commands](https://gnome.pages.gitlab.gnome.org/orca/help/commands.html)
 * [Accessibility in GNOME](https://help.gnome.org/users/gnome-help/stable/a11y.html)
 * [Accessibility in KDE](https://userbase.kde.org/Accessibility/Getting_Started)

## Useful Links
{: useful-links}

 * [Orca's Issue tracker](https://gitlab.gnome.org/GNOME/orca/issues) on GNOME GitLab
 * ["orca" Mailing List](https://www.freelists.org/list/orca) on Freelists. You can [subscribe via email](mailto:orca-request@freelists.org?subject=subscribe) also.